﻿using Entidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utilerias;

namespace SuperZapatosDesktop
{
    public partial class frmStores : Form
    {
        public frmStores()
        {
            InitializeComponent();
        }

        #region Botones

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            loadStores();
            setUI();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveStore();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            setUI();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteStore();
        }

        #endregion

        #region Eventos

        private void frmStores_Load(object sender, EventArgs e)
        {
            loadStores();
            setUI();
        }

        private void dgTiendas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (dgTiendas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    dgTiendas.CurrentRow.Selected = true;

                    txtId.Text = dgTiendas.Rows[e.RowIndex].Cells["id"].FormattedValue.ToString();
                    txtName.Text = dgTiendas.Rows[e.RowIndex].Cells["name"].FormattedValue.ToString();
                    txtAddress.Text = dgTiendas.Rows[e.RowIndex].Cells["address"].FormattedValue.ToString();
                }
            }
        }

        #endregion

        #region Metodos Y Funciones

        private void loadStores()
        {
            try
            {
                dgTiendas.PopulateGrid<cls_Stores>(null);
                dgTiendas.Rows.Clear();

                HttpUtil httpUtil = new HttpUtil();

                string httpUri = ConfigurationManager.AppSettings["ApiUrl"] + "/stores";
                string httpVerb = "GET";
                string httpContentType = "application/json";

                string sResult = httpUtil.SendHttp(httpUri, httpVerb, httpContentType);

                cls_Response oResult = JsonSerializer.Deserialize<cls_Response>(sResult);

                if (oResult.success && oResult.total_elements > 0)
                {
                    dgTiendas.PopulateGrid<cls_Stores>(oResult.stores);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al cargar la información");
            }
        }

        private void saveStore()
        {
            DialogResult d;
            d = MessageBox.Show("Seguro que quiere guardar el registro", "Super Zapatos", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (d == DialogResult.Yes)
            {
                try
                {
                    HttpUtil httpUtil = new HttpUtil();

                    string httpUri = ConfigurationManager.AppSettings["ApiUrl"] + "/store";
                    string httpVerb = "POST";
                    string httpContentType = "application/json";

                    cls_Stores store = new cls_Stores
                    {
                        id = txtId.Text.ParseIntNull(),
                        name = txtName.Text.Trim(),
                        address = txtAddress.Text.Trim()
                    };

                    string sResult = httpUtil.SendHttp(httpUri, httpVerb, httpContentType, JsonSerializer.Serialize(store));

                    cls_Response oResult = JsonSerializer.Deserialize<cls_Response>(sResult);

                    if (oResult.success)
                    {
                        MessageBox.Show("Registro guardado de forma correcta");
                        loadStores();
                        setUI();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo completar la acción");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error al guardar la información");
                }
            }
        }

        private void deleteStore()
        {
            DialogResult d;
            d = MessageBox.Show("Seguro que quiere borrar el registro", "Super Zapatos", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (d == DialogResult.Yes)
            {
                try
                {
                    if (!String.IsNullOrEmpty(txtId.Text))
                    {
                        HttpUtil httpUtil = new HttpUtil();

                        string httpUri = ConfigurationManager.AppSettings["ApiUrl"] + "/store/" + txtId.Text.Trim();
                        string httpVerb = "DELETE";
                        string httpContentType = "application/json";

                        string sResult = httpUtil.SendHttp(httpUri, httpVerb, httpContentType);

                        cls_Response oResult = JsonSerializer.Deserialize<cls_Response>(sResult);

                        if (oResult.success)
                        {
                            MessageBox.Show("Registro borrado de forma correcta");
                            loadStores();
                        }
                        else
                        {
                            MessageBox.Show("No se pudo completar la acción");
                        }
                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error al borrar la información");
                }
            }
        }

        private void setUI()
        {
            txtId.Text = String.Empty;
            txtName.Text = String.Empty;
            txtAddress.Text = String.Empty;

            txtName.Focus();
        }
        #endregion


    }
}
