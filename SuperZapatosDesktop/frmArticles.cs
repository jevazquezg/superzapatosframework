﻿using Entidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utilerias;

namespace SuperZapatosDesktop
{
    public partial class frmArticles : Form
    {
        public frmArticles()
        {
            InitializeComponent();
        }

        #region Botones

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            loadArticles();
            loadStores();
            setUI();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveArticle();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            setUI();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteArticle();
        }

        #endregion

        #region Eventos

        private void frmArticles_Load(object sender, EventArgs e)
        {
            loadArticles();
            loadStores();
            setUI();
        }

        private void dgArticles_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (dgArticles.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    dgArticles.CurrentRow.Selected = true;

                    var store = new cls_Stores(dgArticles.Rows[e.RowIndex].Cells["store_id"].FormattedValue.ToString().ParseInt(), "", "");

                    txtId.Text = dgArticles.Rows[e.RowIndex].Cells["id"].FormattedValue.ToString();
                    txtName.Text = dgArticles.Rows[e.RowIndex].Cells["name"].FormattedValue.ToString();
                    txtDescription.Text = dgArticles.Rows[e.RowIndex].Cells["description"].FormattedValue.ToString();
                    cbxStore.SelectedItem = store;
                    txtPrice.Value = dgArticles.Rows[e.RowIndex].Cells["price"].FormattedValue.ToString().ParseDecimal();
                    txtTotalShelf.Value = dgArticles.Rows[e.RowIndex].Cells["total_in_shelf"].FormattedValue.ToString().ParseDecimal();
                    txtTotalVault.Value = dgArticles.Rows[e.RowIndex].Cells["total_in_vault"].FormattedValue.ToString().ParseDecimal();
                }
            }
        }

        #endregion

        #region Metodos Y Funciones

        private void loadArticles()
        {
            try
            {
                dgArticles.PopulateGrid<cls_Articles>(null);
                dgArticles.Rows.Clear();

                HttpUtil httpUtil = new HttpUtil();

                string httpUri = ConfigurationManager.AppSettings["ApiUrl"] + "/articles";
                string httpVerb = "GET";
                string httpContentType = "application/json";

                string sResult = httpUtil.SendHttp(httpUri, httpVerb, httpContentType);

                cls_Response oResult = JsonSerializer.Deserialize<cls_Response>(sResult);

                if (oResult.success && oResult.total_elements > 0)
                {
                    dgArticles.PopulateGrid<cls_Articles>(oResult.articles);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al cargar la información");
            }
        }

        private void loadStores()
        {
            try
            {
                HttpUtil httpUtil = new HttpUtil();

                string httpUri = ConfigurationManager.AppSettings["ApiUrl"] + "/stores";
                string httpVerb = "GET";
                string httpContentType = "application/json";

                string sResult = httpUtil.SendHttp(httpUri, httpVerb, httpContentType);

                cls_Response oResult = JsonSerializer.Deserialize<cls_Response>(sResult);

                if (oResult.success && oResult.total_elements > 0)
                {
                    cbxStore.PopulateComboBox<cls_Stores>("id", "name", oResult.stores, 0, "Seleciona una tienda");
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void saveArticle()
        {
            DialogResult d;
            d = MessageBox.Show("Seguro que quiere guardar el registro", "Super Zapatos", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (d == DialogResult.Yes)
            {
                try
                {
                    var store = (cls_Stores)cbxStore.SelectedItem;

                    HttpUtil httpUtil = new HttpUtil();

                    string httpUri = ConfigurationManager.AppSettings["ApiUrl"] + "/article";
                    string httpVerb = "POST";
                    string httpContentType = "application/json";

                    cls_Articles article = new cls_Articles
                    {
                        id = txtId.Text.ParseIntNull(),
                        name = txtName.Text.Trim(),
                        description = txtDescription.Text.Trim(),
                        price = txtPrice.Value.ToString().ParseDecimal(),
                        total_in_shelf = txtTotalShelf.Value.ToString().ParseInt(),
                        total_in_vault = txtTotalVault.Value.ToString().ParseInt(),
                        store_id = (int)store.id
                    };

                    string sResult = httpUtil.SendHttp(httpUri, httpVerb, httpContentType, JsonSerializer.Serialize(article));

                    cls_Response oResult = JsonSerializer.Deserialize<cls_Response>(sResult);

                    if (oResult.success)
                    {
                        MessageBox.Show("Registro guardado de forma correcta");
                        loadArticles();
                        setUI();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo completar la acción");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error al guardar la información");
                }
            }
        }

        private void deleteArticle()
        {
            DialogResult d;
            d = MessageBox.Show("Seguro que quiere borrar el registro", "Super Zapatos", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (d == DialogResult.Yes)
            {
                try
                {
                    if (!String.IsNullOrEmpty(txtId.Text))
                    {
                        HttpUtil httpUtil = new HttpUtil();

                        string httpUri = ConfigurationManager.AppSettings["ApiUrl"] + "/article/" + txtId.Text.Trim();
                        string httpVerb = "DELETE";
                        string httpContentType = "application/json";

                        string sResult = httpUtil.SendHttp(httpUri, httpVerb, httpContentType);

                        cls_Response oResult = JsonSerializer.Deserialize<cls_Response>(sResult);

                        if (oResult.success)
                        {
                            MessageBox.Show("Registro borrado de forma correcta");
                            loadArticles();
                        }
                        else
                        {
                            MessageBox.Show("No se pudo completar la acción");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocurrió un error al borrar la información");
                }
            }
        }

        private void setUI()
        {
            txtId.Text = String.Empty;
            txtName.Text = String.Empty;
            txtDescription.Text = String.Empty;
            cbxStore.SelectedIndex = 0;
            txtPrice.Value = 0.00m;
            txtTotalShelf.Value = 0;
            txtTotalVault.Value = 0;

            txtName.Focus();
        }

        #endregion


    }
}
