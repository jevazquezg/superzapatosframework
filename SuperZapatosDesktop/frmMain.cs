﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperZapatosDesktop
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        #region Botones

        private void btnArticles_Click(object sender, EventArgs e)
        {
            openArticles();

        }

        private void btnStores_Click(object sender, EventArgs e)
        {
            openStores();

        }

        #endregion

        #region Eventos

        #endregion

        #region Metodos Y Funciones


        private void openArticles()
        {
            loadForm(new frmArticles());
        }

        private void openStores()
        {
            loadForm(new frmStores());
        }

        private void loadForm(Form FormHijo)
        {
            if (pnlBody.Controls.Count > 0)
                pnlBody.Controls.RemoveAt(0);
            Form fh = FormHijo as Form;
            AddOwnedForm(fh);
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlBody.Controls.Add(fh);
            this.pnlBody.Tag = fh;
            fh.Show();
        }

        #endregion


    }
}
