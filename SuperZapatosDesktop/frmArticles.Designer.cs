﻿
namespace SuperZapatosDesktop
{
    partial class frmArticles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmArticles));
            this.pnlForm = new System.Windows.Forms.Panel();
            this.dgArticles = new System.Windows.Forms.DataGridView();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.PictureBox();
            this.btnAdd = new System.Windows.Forms.PictureBox();
            this.btnSave = new System.Windows.Forms.PictureBox();
            this.btnRefresh = new System.Windows.Forms.PictureBox();
            this.pnlTitle = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnlFields = new System.Windows.Forms.Panel();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.cbxStore = new System.Windows.Forms.ComboBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblTotalShelf = new System.Windows.Forms.Label();
            this.lblTotalVault = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.NumericUpDown();
            this.txtTotalShelf = new System.Windows.Forms.NumericUpDown();
            this.txtTotalVault = new System.Windows.Forms.NumericUpDown();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total_in_shelf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total_in_vault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.store_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgArticles)).BeginInit();
            this.pnlControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).BeginInit();
            this.pnlTitle.SuspendLayout();
            this.pnlFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalShelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVault)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlForm
            // 
            this.pnlForm.BackColor = System.Drawing.Color.White;
            this.pnlForm.Controls.Add(this.dgArticles);
            this.pnlForm.Controls.Add(this.pnlFields);
            this.pnlForm.Controls.Add(this.pnlControls);
            this.pnlForm.Controls.Add(this.pnlTitle);
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.Location = new System.Drawing.Point(0, 0);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Size = new System.Drawing.Size(990, 745);
            this.pnlForm.TabIndex = 0;
            // 
            // dgArticles
            // 
            this.dgArticles.AllowUserToAddRows = false;
            this.dgArticles.AllowUserToDeleteRows = false;
            this.dgArticles.AllowUserToOrderColumns = true;
            this.dgArticles.AllowUserToResizeColumns = false;
            this.dgArticles.AllowUserToResizeRows = false;
            this.dgArticles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgArticles.BackgroundColor = System.Drawing.Color.White;
            this.dgArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgArticles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.description,
            this.price,
            this.total_in_shelf,
            this.total_in_vault,
            this.store_id});
            this.dgArticles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgArticles.Location = new System.Drawing.Point(0, 250);
            this.dgArticles.Name = "dgArticles";
            this.dgArticles.Size = new System.Drawing.Size(990, 495);
            this.dgArticles.TabIndex = 7;
            this.dgArticles.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgArticles_CellClick);
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.btnDelete);
            this.pnlControls.Controls.Add(this.btnAdd);
            this.pnlControls.Controls.Add(this.btnSave);
            this.pnlControls.Controls.Add(this.btnRefresh);
            this.pnlControls.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlControls.Location = new System.Drawing.Point(0, 70);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(990, 40);
            this.pnlControls.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(148, 5);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(30, 30);
            this.btnDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnDelete.TabIndex = 7;
            this.btnDelete.TabStop = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(105, 5);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(30, 30);
            this.btnAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnAdd.TabIndex = 6;
            this.btnAdd.TabStop = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(62, 5);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(30, 30);
            this.btnSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnSave.TabIndex = 5;
            this.btnSave.TabStop = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(19, 5);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(30, 30);
            this.btnRefresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.TabStop = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // pnlTitle
            // 
            this.pnlTitle.Controls.Add(this.lblTitle);
            this.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitle.Location = new System.Drawing.Point(0, 0);
            this.pnlTitle.Name = "pnlTitle";
            this.pnlTitle.Size = new System.Drawing.Size(990, 70);
            this.pnlTitle.TabIndex = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(12, 17);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(127, 37);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Artículos";
            // 
            // pnlFields
            // 
            this.pnlFields.Controls.Add(this.txtTotalVault);
            this.pnlFields.Controls.Add(this.txtTotalShelf);
            this.pnlFields.Controls.Add(this.txtPrice);
            this.pnlFields.Controls.Add(this.lblTotalVault);
            this.pnlFields.Controls.Add(this.lblTotalShelf);
            this.pnlFields.Controls.Add(this.lblPrice);
            this.pnlFields.Controls.Add(this.cbxStore);
            this.pnlFields.Controls.Add(this.lblStore);
            this.pnlFields.Controls.Add(this.txtDescription);
            this.pnlFields.Controls.Add(this.txtName);
            this.pnlFields.Controls.Add(this.txtId);
            this.pnlFields.Controls.Add(this.lblDescription);
            this.pnlFields.Controls.Add(this.lblName);
            this.pnlFields.Controls.Add(this.lblId);
            this.pnlFields.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFields.Location = new System.Drawing.Point(0, 110);
            this.pnlFields.Name = "pnlFields";
            this.pnlFields.Size = new System.Drawing.Size(990, 140);
            this.pnlFields.TabIndex = 5;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(403, 37);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(327, 20);
            this.txtDescription.TabIndex = 2;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(148, 37);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(223, 20);
            this.txtName.TabIndex = 1;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(19, 37);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 0;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(400, 20);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = "Descripción";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(145, 20);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(44, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Nombre";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(16, 20);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(18, 13);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "ID";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.Location = new System.Drawing.Point(760, 20);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(40, 13);
            this.lblStore.TabIndex = 3;
            this.lblStore.Text = "Tienda";
            // 
            // cbxStore
            // 
            this.cbxStore.FormattingEnabled = true;
            this.cbxStore.Location = new System.Drawing.Point(763, 37);
            this.cbxStore.Name = "cbxStore";
            this.cbxStore.Size = new System.Drawing.Size(187, 21);
            this.cbxStore.TabIndex = 3;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(16, 78);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(37, 13);
            this.lblPrice.TabIndex = 5;
            this.lblPrice.Text = "Precio";
            // 
            // lblTotalShelf
            // 
            this.lblTotalShelf.AutoSize = true;
            this.lblTotalShelf.Location = new System.Drawing.Point(145, 78);
            this.lblTotalShelf.Name = "lblTotalShelf";
            this.lblTotalShelf.Size = new System.Drawing.Size(114, 13);
            this.lblTotalShelf.TabIndex = 7;
            this.lblTotalShelf.Text = "Cantidad en Mostrador";
            // 
            // lblTotalVault
            // 
            this.lblTotalVault.AutoSize = true;
            this.lblTotalVault.Location = new System.Drawing.Point(277, 78);
            this.lblTotalVault.Name = "lblTotalVault";
            this.lblTotalVault.Size = new System.Drawing.Size(108, 13);
            this.lblTotalVault.TabIndex = 9;
            this.lblTotalVault.Text = "Cantidad en Almacen";
            // 
            // txtPrice
            // 
            this.txtPrice.DecimalPlaces = 2;
            this.txtPrice.Location = new System.Drawing.Point(19, 95);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(100, 20);
            this.txtPrice.TabIndex = 10;
            // 
            // txtTotalShelf
            // 
            this.txtTotalShelf.Location = new System.Drawing.Point(148, 95);
            this.txtTotalShelf.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtTotalShelf.Name = "txtTotalShelf";
            this.txtTotalShelf.Size = new System.Drawing.Size(100, 20);
            this.txtTotalShelf.TabIndex = 5;
            // 
            // txtTotalVault
            // 
            this.txtTotalVault.Location = new System.Drawing.Point(280, 94);
            this.txtTotalVault.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txtTotalVault.Name = "txtTotalVault";
            this.txtTotalVault.Size = new System.Drawing.Size(100, 20);
            this.txtTotalVault.TabIndex = 6;
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.FillWeight = 71.06599F;
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.FillWeight = 104.8223F;
            this.name.HeaderText = "Nombre";
            this.name.Name = "name";
            // 
            // description
            // 
            this.description.DataPropertyName = "description";
            this.description.FillWeight = 104.8223F;
            this.description.HeaderText = "Descripción";
            this.description.Name = "description";
            // 
            // price
            // 
            this.price.DataPropertyName = "price";
            this.price.FillWeight = 104.8223F;
            this.price.HeaderText = "Precio";
            this.price.Name = "price";
            // 
            // total_in_shelf
            // 
            this.total_in_shelf.DataPropertyName = "total_in_shelf";
            this.total_in_shelf.FillWeight = 104.8223F;
            this.total_in_shelf.HeaderText = "Cantidad en Mostrador";
            this.total_in_shelf.Name = "total_in_shelf";
            // 
            // total_in_vault
            // 
            this.total_in_vault.DataPropertyName = "total_in_vault";
            this.total_in_vault.FillWeight = 104.8223F;
            this.total_in_vault.HeaderText = "Cantidad en Almacen";
            this.total_in_vault.Name = "total_in_vault";
            // 
            // store_id
            // 
            this.store_id.DataPropertyName = "store_id";
            this.store_id.FillWeight = 104.8223F;
            this.store_id.HeaderText = "Tienda";
            this.store_id.Name = "store_id";
            // 
            // frmArticles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 745);
            this.Controls.Add(this.pnlForm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 70);
            this.Name = "frmArticles";
            this.Text = "frmArticles";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmArticles_Load);
            this.pnlForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgArticles)).EndInit();
            this.pnlControls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).EndInit();
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            this.pnlFields.ResumeLayout(false);
            this.pnlFields.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalShelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalVault)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlForm;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.Panel pnlTitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox btnDelete;
        private System.Windows.Forms.PictureBox btnAdd;
        private System.Windows.Forms.PictureBox btnSave;
        private System.Windows.Forms.PictureBox btnRefresh;
        private System.Windows.Forms.DataGridView dgArticles;
        private System.Windows.Forms.Panel pnlFields;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.ComboBox cbxStore;
        private System.Windows.Forms.Label lblStore;
        private System.Windows.Forms.Label lblTotalVault;
        private System.Windows.Forms.Label lblTotalShelf;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.NumericUpDown txtTotalVault;
        private System.Windows.Forms.NumericUpDown txtTotalShelf;
        private System.Windows.Forms.NumericUpDown txtPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn total_in_shelf;
        private System.Windows.Forms.DataGridViewTextBoxColumn total_in_vault;
        private System.Windows.Forms.DataGridViewTextBoxColumn store_id;
    }
}