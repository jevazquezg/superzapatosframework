﻿using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    /// <summary>
    /// Capa de Acceso a Datos (DAL) para la entidad Stores
    /// </summary>
    public static class DAL_Stores
    {

        /// <summary>
        /// Obtiene el listado de tiendas de la DB
        /// </summary>
        public static List<stores> ListStores()
        {
            using (var db = new MyDBContext())
            {
                return db.stores.ToList();
            }
        }

        /// <summary>
        /// Agrega o Actualiza un registro en la taba Stores de la DB
        /// </summary>
        /// <param name="store">Registro a agregar o editar</param>
        /// <param name="query">Tipo de acción 1: Insert, 2:Update</param>
        public static bool AddOrEditStore(stores store, Query query)
        {
            bool flag = false;

            using (var db = new MyDBContext())
            {
                switch (query)
                {
                    case Query.Insert:

                        db.stores.Add(store);
                        db.SaveChanges();

                        flag = true;

                        break;

                    case Query.Update:

                        var result = db.stores.SingleOrDefault(b => b.id == store.id);
                        if (result != null)
                        {
                            result.name = store.name;
                            result.address = store.address;
                            db.SaveChanges();

                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }

                        break;
                }

                return flag;
            }
        }

        /// <summary>
        /// Elimina un registro en la taba Stores de la DB
        /// </summary>
        public static bool DeleteStore(int id)
        {
            bool flag = false;

            using (var db = new MyDBContext())
            {
                var result = db.stores.SingleOrDefault(b => b.id == id);
                if (result != null)
                {
                    db.stores.Remove(result);
                    db.SaveChanges();

                    flag = true;
                }
                else
                {
                    flag = false;
                }


                return flag;
            }
        }

    }
}
