﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class MyDBContext : DbContext
    {

        #region Atributos

        public DbSet<articles> articles { get; set; }
        public DbSet<stores> stores { get; set; }

        #endregion

        #region Constructores

        public MyDBContext() : base("strSuperZapatosDB") { }

        #endregion

    }
}
