﻿namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.articles", "name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.articles", "description", c => c.String(maxLength: 250));
            AlterColumn("dbo.stores", "name", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.stores", "name", c => c.String());
            AlterColumn("dbo.articles", "description", c => c.String());
            AlterColumn("dbo.articles", "name", c => c.String());
        }
    }
}
