﻿using Entidad;
using Negocio;
using SuperZapatosApi.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Web.Http;

namespace SuperZapatosApi.Controllers
{
    public class ServiceController : ApiController
    {

        #region Stores

        [Route("services/stores")]
        [AcceptVerbs("GET")]
        //[BasicAuthentication]
        public cls_Response getStores()
        {
            JsonSerializerOptions options = new JsonSerializerOptions()
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
            };
            cls_Response oResponse = new cls_Response();

            try
            {
                List<cls_Stores> responseStores = NE_Stores.ListStores();

                // Genera la estructura de respuesta
                oResponse.stores = responseStores;
                oResponse.total_elements = responseStores.Count();
                oResponse.success = true;
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            //return JsonSerializer.Serialize<cls_Response>(oResponse, options);
            return oResponse;
        }

        [Route("services/store")]
        [AcceptVerbs("POST")]
        public cls_Response setStore([FromBody] cls_Stores store)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                Respuesta respuesta = NE_Stores.AddOrEditStore(store);

                // Genera la estructura de respuesta
                switch (respuesta)
                {
                    case Respuesta.Ok:
                        oResponse.success = true;
                        break;
                    case Respuesta.Error:
                        oResponse.success = false;
                        oResponse.error_mesg = "Bad request";
                        oResponse.error_code = 400;
                        break;
                }

            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [Route("services/store/{id}")]
        [AcceptVerbs("DELETE")]
        public cls_Response delStore([FromUri] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                Respuesta respuesta = NE_Stores.DeleteStore(id);

                // Genera la estructura de respuesta
                switch (respuesta)
                {
                    case Respuesta.Ok:
                        oResponse.success = true;
                        break;
                    case Respuesta.Error:
                        oResponse.success = false;
                        oResponse.error_mesg = "Bad request";
                        oResponse.error_code = 400;
                        break;
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        #endregion


        #region Articles

        [Route("services/articles")]
        [AcceptVerbs("GET")]
        //[BasicAuthentication]
        public cls_Response getArticles()
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                List<cls_Articles> responseArticles = NE_Articles.ListArticles();

                // Genera la estructura de respuesta
                oResponse.articles = responseArticles;
                oResponse.total_elements = responseArticles.Count();
                oResponse.success = true;
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [Route("services/articles/stores/{id}")]
        [AcceptVerbs("GET")]
        //[BasicAuthentication]
        public cls_Response getArticleById([FromUri] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                List<cls_Articles> responseArticles = NE_Articles.getArticleByStoreID(id);

                // Genera la estructura de respuesta
                oResponse.articles = responseArticles;
                oResponse.total_elements = responseArticles.Count();
                oResponse.success = true;
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [Route("services/article")]
        [AcceptVerbs("POST")]
        public cls_Response setArticle([FromBody] cls_Articles article)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                Respuesta respuesta = NE_Articles.AddOrEditArticles(article);

                // Genera la estructura de respuesta
                switch (respuesta)
                {
                    case Respuesta.Ok:
                        oResponse.success = true;
                        break;
                    case Respuesta.Error:
                        oResponse.success = false;
                        oResponse.error_mesg = "Bad request";
                        oResponse.error_code = 400;
                        break;
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        [Route("services/article/{id}")]
        [AcceptVerbs("DELETE")]
        public cls_Response delArticle([FromUri] int id)
        {
            cls_Response oResponse = new cls_Response();

            try
            {
                Respuesta respuesta = NE_Articles.DeleteArticle(id);

                // Genera la estructura de respuesta
                switch (respuesta)
                {
                    case Respuesta.Ok:
                        oResponse.success = true;
                        break;
                    case Respuesta.Error:
                        oResponse.success = false;
                        oResponse.error_mesg = "Bad request";
                        oResponse.error_code = 400;
                        break;
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Respuesta
                oResponse.success = false;
                oResponse.error_mesg = "Server Error";
                oResponse.error_code = 500;
            }

            return oResponse;
        }

        #endregion

    }
}