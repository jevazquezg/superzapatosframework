﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Utilerias
{
    public class HttpUtil
    {

        public static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string SendHttp(string HttpUri, string HttpMethod, string HttpContentType, string HttpContent = null)
        {
            string result = null;

            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

                Uri myUri = new Uri(HttpUri);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(myUri);
                httpWebRequest.ContentType = HttpContentType;
                httpWebRequest.Method = HttpMethod;

                //NetworkCredential myNetworkCredential = new NetworkCredential(HttpUser, HttpPassword);
                //CredentialCache myCredentialCache = new CredentialCache();
                //myCredentialCache.Add(myUri, "Basic", myNetworkCredential);

                httpWebRequest.PreAuthenticate = false;
                //httpWebRequest.Credentials = myCredentialCache;

                if (HttpMethod == "POST" || HttpMethod == "PUT")
                {
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(HttpContent);
                    }
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

    }
}
