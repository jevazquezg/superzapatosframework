﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utilerias
{
    public static class Extensions
    {

        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR ENTERO
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static int ParseInt(this string sCadena)
        {
            var iValorEntero = 0;
            return !int.TryParse(sCadena, out iValorEntero)
                    ? 0
                    : iValorEntero;
        }

        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR ENTERO NULL
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static int? ParseIntNull(this string sCadena)
        {
            var iValorEntero = 0;
            return !int.TryParse(sCadena, out iValorEntero)
                    ? (int?)null
                    : iValorEntero;
        }

        /// <summary>
        /// EXTENSION PARA PODER CONVERTIR UN VALOR STRING A UN VALOR DECIMAL
        /// </summary>
        /// <param name="pValor"></param>
        /// <returns></returns>
        public static decimal ParseDecimal(this string sCadena)
        {
            var dcValorEntero = 0m;
            return !decimal.TryParse(sCadena, out dcValorEntero)
                    ? 0m
                    : dcValorEntero;
        }

        /// <summary>
        /// EXTENSIÓN PARA LLENAR UN RADGRID CON UNA LISTA
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pGrid"></param>
        /// <param name="pListDataSorce"></param>
        public static void PopulateGrid<T>(this DataGridView pGrid, List<T> pListDataSorce)
        {
            pGrid.DataSource = pListDataSorce;
        }


        /// <summary>
        /// EXTENSIÓN PARA LLENAR UN COMBOBOX CON UNA LISTA
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pList"></param>
        /// <param name="pValue"></param>
        /// <param name="pText"></param>
        /// <param name="pListDataSorce"></param>
        /// <param name="pValueMessage"></param>
        /// <param name="pTextMessage"></param>
        public static void PopulateComboBox<T>(this ComboBox pList,
                                                        string pValue,
                                                        string pText,
                                                    List<T> pListDataSorce,
                                                        int? pValueMessage = null,
                                                        string pTextMessage = "")
        {
            pList.Items.Clear();
            pList.Items.Add(pTextMessage);

            foreach (var item in pListDataSorce)
            {
                pList.Items.Add(item);
            }
            pList.ValueMember = pValue;
            pList.DisplayMember = pText;

            pList.SelectedIndex = (int)pValueMessage;
        }

    }

}
