﻿using Datos;
using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    /// <summary>
    /// Capa de Negocio para la entidad Stores
    /// </summary>
    public static class NE_Stores
    {

        /// <summary>
        /// Listado de tiendas
        /// </summary>
        public static List<cls_Stores> ListStores()
        {
            List<cls_Stores> clsStores = new List<cls_Stores>();
            List<stores> stores = DAL_Stores.ListStores();

            try
            {
                foreach (var store in stores)
                {
                    clsStores.Add(new cls_Stores(store.id, store.name, store.address));
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsStores;
        }

        /// <summary>
        /// Agrega o Actualiza un registro en la tabla Stores
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public static Respuesta AddOrEditStore(cls_Stores store)
        {
            if (store != null)
            {
                if (DAL_Stores.AddOrEditStore(new stores() { id = store.id.HasValue ? (int)store.id : 0, name = store.name, address = store.address }, store.id.HasValue ? Query.Update : Query.Insert))
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }

        /// <summary>
        /// Elimina un registro en la taba Stores
        /// </summary>
        public static Respuesta DeleteStore(int? id)
        {
            if (id != null)
            {
                if (DAL_Stores.DeleteStore((int)id))
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }
    }
}
