﻿using Datos;
using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    /// <summary>
    /// Capa de Negocio para la entidad Articles
    /// </summary>
    public static class NE_Articles
    {

        /// <summary>
        /// Listado de artículos
        /// </summary>
        public static List<cls_Articles> ListArticles()
        {
            List<cls_Articles> clsArticles = new List<cls_Articles>();
            List<articles> articles = DAL_Articles.ListArticles();

            try
            {
                foreach (var article in articles)
                {
                    clsArticles.Add(new cls_Articles(article.id, article.name, article.description, article.price, article.total_in_shelf, article.total_in_vault, article.store_id));
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsArticles;
        }

        /// <summary>
        /// Consulta artículo por ID de tienda
        /// </summary>
        public static List<cls_Articles> getArticleByStoreID(int id)
        {
            List<cls_Articles> clsArticles = new List<cls_Articles>();
            List<articles> articles = DAL_Articles.getArticleByStoreID(id);

            try
            {
                foreach (var article in articles)
                {
                    clsArticles.Add(new cls_Articles(article.id, article.name, article.description, article.price, article.total_in_shelf, article.total_in_vault, article.store_id));
                }
            }
            catch (Exception ex)
            {
                // Log de Excepciones

                // Se envia la excepcion al controlador para armar la respuesta
                throw ex;
            }

            return clsArticles;
        }

        /// <summary>
        /// Agrega o Actualiza un registro en la tabla Articles
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        public static Respuesta AddOrEditArticles(cls_Articles article)
        {
            if (article != null)
            {
                if (DAL_Articles.AddOrEditArticle(new articles()
                {
                    id = article.id.HasValue ? (int)article.id : 0,
                    name = article.name,
                    description = article.description,
                    price = article.price,
                    total_in_shelf = article.total_in_shelf,
                    total_in_vault = article.total_in_vault,
                    store_id = article.store_id
                }, article.id.HasValue ? Query.Update : Query.Insert))
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }

        /// <summary>
        /// Elimina un registro en la taba Articles
        /// </summary>
        public static Respuesta DeleteArticle(int? id)
        {
            if (id != null)
            {
                if (DAL_Articles.DeleteArticle((int)id))
                {
                    return Respuesta.Ok;
                }
                else
                {
                    return Respuesta.Error;
                }
            }
            else
            {
                return Respuesta.Error;
            }
        }
    }
}
